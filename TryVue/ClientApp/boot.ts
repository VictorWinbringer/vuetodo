import './css/site.css';
import 'bootstrap';
import Vue from 'vue';
import VueRouter from 'vue-router';
import { store, RootState, TodoItem } from './store';
Vue.use(VueRouter);
Vue.component("todo-table", require('./components/todoTable/todoTable.vue.html'));
const routes = [
    { path: '/', component: require('./components/home/home.vue.html') },
    { path: '/counter', component: require('./components/counter/counter.vue.html') },
    { path: '/fetchdata', component: require('./components/fetchdata/fetchdata.vue.html') },
    { path: '/todo', component: require('./components/todo/todo.vue.html') },
    { path: '/todo/delete/:id', component: require('./components/delete/delete.vue.html') },
];

new Vue({
    el: '#app-root',
    store:store,
    router: new VueRouter({ mode: 'history', routes: routes }),
    render: h => h(require('./components/app/app.vue.html'))
});
