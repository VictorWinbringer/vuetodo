﻿import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

Vue.use(Vuex);

export interface TodoItem {
    id: number;
    name: string;
    isComplite: boolean;
}


export interface RootState {  
    items: TodoItem[];
    current: TodoItem;
}

const key: string = "TodoVueItems";

export const store = new Vuex.Store<RootState>({
    state: {
        items: [], current: { id: 0, name: "", isComplite: false }
    },
    getters: {
        TODOS: state => {
            return state.items;
        },
        TODO: s => {
            return s.current;
        }
    },
    mutations: {
        SET_TODO: (state, payload: TodoItem[]) => {
            state.items = payload;
        },
        ADD_TODO: (s, p: TodoItem) => {
            s.items.push(p);
        },
        REMOVE_TODO: (s, p: number) => {
            s.items = s.items.filter(x => x.id != p);
        },
        UPDATE_TODO: (s, p: TodoItem) => {
            let todo = s.items.filter(t => t.id == p.id)[0];
            todo.isComplite = p.isComplite;
            todo.name = p.name;
        },
        SELECT: (s, p: number) => {
            s.current = s.items.filter(x => x.id == p)[0];
        },
        UNSELECT: (s) => {
            s.current = { id: 0, name: "", isComplite: false };
        }
    },
    actions: {
        READ_ALL: (c) => {
            var json = localStorage.getItem(key);
            if (json !== null) {
                let items:TodoItem[] = JSON.parse(json);
                c.commit('SET_TODO',items)
            }
        },
        CREATE: (c, p: TodoItem) => {
            c.commit('ADD_TODO', p);
            localStorage.setItem(key, JSON.stringify(c.state.items));
        },
        UPDATE: (c, p: TodoItem) => {
            c.commit('UPDATE_TODO', p);
            localStorage.setItem(key, JSON.stringify(c.state.items));
        },
        DELETE: (c, p: number) => {
            c.commit('REMOVE_TODO', p);
            localStorage.setItem(key, JSON.stringify(c.state.items));
        },
        SELECT: (c, p: number) => {
            c.commit('SELECT', p);
        },
        UNSELECT: (c) => {
            c.commit('UNSELECT');
        }
    },
});