﻿import Vue from 'vue';
import Vuex from 'vuex';
import { Component } from 'vue-property-decorator';

interface TodoItem {
    id: number;
    name: string;
    isComplite: boolean;
}

@Component
export default class DeleteComponent extends Vue {
  
    mounted() {
          this.$root.$store.dispatch('READ_ALL');
          this.$root.$store.dispatch('SELECT',this.$route.params.id);
    }
   
    current(){
        return this.$root.$store.getters.TODO;
    }
    deleteTodo() {
        this.$root.$store.dispatch('DELETE',this.current().id);
        this.$root.$store.dispatch('UNSELECT');
        this.$router.push("/todo");
    }
}