﻿import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class TodoItemsComponent extends Vue {

    newTodo: string = "";

    mounted() {
        this.$root.$store.dispatch('READ_ALL');
    }

    current() {
        return this.$root.$store.getters.TODO;
    }

    items() {
        return this.$root.$store.getters.TODOS;
    }

    save() {
        this.$root.$store.dispatch('UPDATE', this.current());
        this.$root.$store.dispatch('UNSELECT');
    }

    add() {
        let date = new Date();
        this.$root.$store.dispatch('CREATE', { id: date.valueOf(), name: this.newTodo, isComplite: false });
        this.newTodo = "";
    }

    canselEdit() {
        this.$root.$store.dispatch('UNSELECT');
    }

    selectRow(id: number) {
        this.$root.$store.dispatch('SELECT', id);
    }
}