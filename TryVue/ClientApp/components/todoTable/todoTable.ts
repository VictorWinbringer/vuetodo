﻿import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';
import { TodoItem } from '../../store';

@Component
export default class TodoTableComponent extends Vue {
    @Prop()
    public readonly current: any;
    @Prop()
    public readonly select: any;
    @Prop()
    public readonly items: any;

    mounted() {
        console.log(this.current);
        console.log(this.select);
        console.log(this.items);
    }

    getRowClass(id: number): string {
        return this.current == id ? "info" : "";
    }
}